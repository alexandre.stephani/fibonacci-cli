import Fibonacci from "./fibonacci-cli";

describe('Units', () => {
    
    test('should return 0 from fibonacci(1)', () => {
      const fibonnaciNumber =  new Fibonacci(1);
      expect(fibonnaciNumber.list()).toEqual(0);
    });

    test('should return 1 from fibonacci(2)', () => {
      const fibonnaciNumber =  new Fibonacci(2);
      expect(fibonnaciNumber.list()).toEqual(1);
    });

    test('should return 1 from fibonacci(3)', () => {
      const fibonnaciNumber =  new Fibonacci(3);
      expect(fibonnaciNumber.list()).toEqual(1);
    });

    test('should return 2 from fibonacci(4)', () => {
      const fibonnaciNumber =  new Fibonacci(4);
      expect(fibonnaciNumber.list()).toEqual(2);
    });

    test('should return 3 from fibonacci(5)', () => {
      const fibonnaciNumber =  new Fibonacci(5);
      expect(fibonnaciNumber.list()).toEqual(3);
    });

    test('should return 34 from fibonacci(10)', () => {
      const fibonnaciNumber =  new Fibonacci(10);
      expect(fibonnaciNumber.list()).toEqual(34);
    });   
});
