#!/usr/bin/env node

const yargs = require('yargs');
const Fibonacci = require('./fibonacci-cli.js');

module.exports = () => {
  const options = yargs
   .usage("Usage: -n <fibonacci-cli>")
   .option("n", { alias: "fibonacci-n-number", describe: "The n number of Fibonacci's sequence", type: "number", demandOption: true })
   .argv;

  const nFibonacci = new Fibonacci(options.n);
    
  console.log(nFibonacci.list());
}

