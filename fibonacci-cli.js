const fibonacciStarters = [0, 1]

let fibonacciSequence = fibonacciStarters;
let fibonacciArrayIndex = 0;

class Fibonacci {
    
    constructor(fibonacciSequenceNumber = 0) {
        for(let i = 2; i < fibonacciSequenceNumber; i++) {
            fibonacciSequence[i] = fibonacciSequence[i-1] + fibonacciSequence[i-2];
        }
        fibonacciArrayIndex = fibonacciSequenceNumber;
    }

    list() { 
        if (fibonacciArrayIndex <= 0) {
            return -1
        } else {
            return fibonacciSequence[fibonacciArrayIndex-1]
        }
    }
}

module.exports = Fibonacci;